import java.util.*;

public class PacketMean {

    public static int nbrSent(int size, double p) {
        int counter = 0;
        do
            counter++;
        while (!packetOK(size, p));
        return counter;
    }

    public static boolean packetOK(int size, double p) {
	boolean ok = true;
        Random rnd = new Random(); //Random number generator
	for(int i = 0; i != size; i++){
		if(rnd.nextDouble() < p){
			ok = false;
			break;
		}
	}
	return ok;
    }

    public static void main(String[] args) {
        double p = 0.001;  //Probability that one bit is wrong
	int header = 160;

	int[] sizes = new int[]{100, 200, 400, 500, 1000, 2000};
	for(int i = 0; i != sizes.length; i++){
		int d = sizes[i];
		int antal = 20000 / d;
		int size = d + 160; // d + h
		double mean = calculateMean(p, size);
		double total = mean * antal * size;
		System.out.println("d: " + d + ", antal: " + antal + ", mean: " + mean + ", total: " + total);
	}
    }

    /**
    * Calculate the mean number of packets that have to be sent
    */
    public static double calculateMean(double p, int size) {
        int counter = 0;
        int nbrExperiments = 100000;
        for (int i = 0; i < nbrExperiments; i++) {
            counter = counter + nbrSent(size, p);
        }
        return 1.0 * counter / nbrExperiments;
    }

}

