import java.io.*;
import java.net.*;
import java.util.*;
public class ServerHelper implements Runnable {

    BufferedReader inFromClient;
    DataOutputStream outToClient;

    ServerHelper(BufferedReader in, DataOutputStream out) {
        inFromClient = in;
        outToClient = out;
    }


    public void run() {
        while (true) {
            try {
                outToClient.writeBytes(Base64.getEncoder().encodeToString(inFromClient.readLine().getBytes()) + '\n');
            }
            catch (IOException exc) {
                System.out.println("This was wrong!");
            }
        }
    }

}
